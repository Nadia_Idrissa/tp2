/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.model;

import java.util.Objects;

/**
 *
 * @author Piaf
 */
public class Todo {

    private String id;
    private String nom;
    private String user_id;
    private Etat statut;

    public Todo() {
    }

//    public Todo(String nuevo, String nuevo0, String nuevo1, String nuevo2) {
//        this.id=nuevo;
//        this.nom=nuevo0;
//        this.statut=nuevo1;
//        this.user_id=nuevo2;
//    }
    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getUser_id() {
        return user_id;
    }

    public Etat getStatut() {
        return statut;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setStatut(String statut) {
        if (statut.equalsIgnoreCase("FINI")) {
            this.statut = Etat.FINI;

        }else if (statut.equalsIgnoreCase("A_FAIRE")){
            this.statut = Etat.A_FAIRE;
            
        }else{
            this.statut = Etat.EN_COURS;
            
        }

    }

    @Override
    public String toString() {
        return "Todo{" + "id=" + id + ", nom=" + nom + ", user_id=" + user_id + ", statut=" + statut + '}';
    }

}
