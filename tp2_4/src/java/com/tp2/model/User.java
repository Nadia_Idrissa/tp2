/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.model;

import java.util.Objects;

/**
 *
 * @author NelletPiaf
 */
public class User {
 private String id;
 private String nom;
 private String courriel;

    public User() {
    }

    public User(String id, String nom, String courriel) {
        this.id = id;
        this.nom = nom;
        this.courriel = courriel;
    }

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getCourriel() {
        return courriel;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", nom=" + nom + ", courriel=" + courriel + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.courriel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.courriel, other.courriel)) {
            return false;
        }
        return true;
    }
 
}
