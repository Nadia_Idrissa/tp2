/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.controller;

import com.tp2.DAO.MyDAO;
import com.tp2.DAO.TodoDao;
import com.tp2.DAO.UserDao;
import com.tp2.factories.ObjectFactory;
import com.tp2.model.Etat;
import com.tp2.model.Todo;

import com.tp2.model.User;
import com.tp2.services.TodoService;
import com.tp2.services.UsrService;
import java.io.IOException;
import java.text.Normalizer.Form;
import java.util.ArrayList;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author NelletPiaf
 */
@Controller
@SessionAttributes("userIdSession")
@RequestMapping("/")
public class projetController {

    private UsrService uService;
    private TodoService tService;
    TodoDao daot;

    public void setUService(UsrService uService) {
        this.uService = uService;
    }

    public void setTService(TodoService todoService) {
        this.tService = todoService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome(ModelMap model) {
        User ulogin = new User("login", "dd", "Courriel");
        model.addAttribute("bienvenue", "Bienvenue au site de projets");
        model.addAttribute("userLogin", ulogin);
        return "home";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpSession session, User userLogin, ModelMap model) throws IOException {
        User uSession = (User) session.getAttribute("connecte");

        List<User> listeDataAllUse = this.uService.getAllusers();
        model.addAttribute("dataAllUser", listeDataAllUse);
        List<Todo> listeDataAllUser = this.tService.getTachesParID(uSession.getId());
        model.put("tachesParUser", listeDataAllUser);
        model.put("tachesParU", listeDataAllUse);
        return "add";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String login(HttpSession session, User userLogin, ModelMap model) throws IOException {
        System.out.println("U login  =====" + userLogin.getCourriel());

        String cou = userLogin.getCourriel();
        model.addAttribute("mycourriel", cou);

        User uSession = new User();
        uSession = uService.getUserParCourriel(cou);
        if (uSession == null) {
            model.put("message", "Courriel invalide");
            uSession = new User("allo", "a", "Courriel");
            model.addAttribute("userLogin", uSession);
            return "home";
        } else {

            session.setAttribute("connecte", uSession);
            model.addAttribute("userLogin", uSession);
            model.put("mycourriel", uSession.getNom());
            List<User> listeDataAllUse = this.uService.getAllusers();
            model.addAttribute("dataAllUser", listeDataAllUse);
            List<Todo> listeDataAllUser = this.tService.getTachesParID(uSession.getId());
            model.put("tachesParUser", listeDataAllUser);
            model.put("tachesParU", listeDataAllUse);
            return "add";
        }
    }

    @RequestMapping(value = "modif", method = RequestMethod.GET, params = {"id"})
    public String modiffTache(@RequestParam("id") String id, User userLogin, ModelMap model) throws IOException {

        Todo t = tService.getProjet(id);
        FormTache ft = new FormTache();
        ft.setId(t.getId());
        ft.setNom(t.getNom());
        ft.setEtat(t.getStatut());
        System.out.println("Nous sommes ici");
        model.addAttribute("formTache", ft);
        model.addAttribute("tache", t);

        return "detailTache";
    }

    @RequestMapping(value = "modif", method = RequestMethod.POST)
    public View submitTache(HttpSession session, FormTache form, ModelMap model) throws IOException {

        System.out.println("ID" + form.getId());
        System.out.println("NOM" + form.getNom());
        System.out.println("Etat" + form.getEtat());
        User u = (User) session.getAttribute("connecte");
        Todo t = new Todo();
        t.setId(form.getId());
        t.setNom(form.getNom());
        t.setStatut(form.getEtat().toString());
        t.setUser_id(u.getId());

        if (tService.setTache(t)) {
            return new RedirectView("/add", true, false);

        }
        return new RedirectView("/modif", true, false);
    }

    @RequestMapping(value = "ajouter", method = RequestMethod.GET)
    public String ajouter(HttpSession session, ModelMap model) throws IOException {
        User uSession = (User) session.getAttribute("connecte");
        FormTache ft = new FormTache();
        model.addAttribute("formTache", ft);

        return "ajouter";
    }

    @RequestMapping(value = "ajouter", method = RequestMethod.POST)
    public View ajouterT(HttpSession session, FormTache form, ModelMap model) throws IOException {
        User uSession = (User) session.getAttribute("connecte");
        Todo t = ObjectFactory.getNewTodo();
        t.setNom(form.getNom());
        t.setStatut(form.getEtat().toString());
        t.setUser_id(uSession.getId());

        if (tService.addProjet(t)) {
            return new RedirectView("/add", true, false);
        }
        return new RedirectView("/ajouter", true, false);
    }

    @RequestMapping(value = "deconnecter", method = RequestMethod.GET)
    public String deconnecter(HttpSession session, ModelMap model) throws IOException {
        System.out.println("Deconnexion");
        User ulogin = new User("login", "dd", "Courriel");      
        model.addAttribute("userLogin", ulogin);
        session.invalidate();
        
        return "home";
    }

    public static class Form {

        private String email = "";

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

    }

    public static class FormTache {

        private String id = "";
        private String nom = "";
        private Etat etat;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public Etat getEtat() {
            return etat;
        }

        public void setEtat(Etat etat) {
            this.etat = etat;
        }

    }
}
