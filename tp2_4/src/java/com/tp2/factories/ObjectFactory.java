/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.factories;


import com.tp2.model.Todo;
import java.util.UUID;

/**
 *
 * @author toute
 */
public class ObjectFactory {

    public static Todo getNewTodo() {
        Todo object = new Todo();
        object.setId(UUID.randomUUID().toString());
        return object;
    }
}
