/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.DAO;


import com.tp2.model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NelletPiaf
 */
public class UserDao extends MyDAO<User>{

    @Override
    public boolean create(User x) {
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("INSERT INTO user (`ID`,`NOM`, `COURRIEL`) "
                                    + "VALUES (?,?, ?)");
            stm.setString(1, x.getId());
            stm.setString(2, x.getNom());
            stm.setString(3, x.getCourriel());
            return stm.executeUpdate()>0;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
          
        }
        return false;
    }

    @Override
    public boolean delete(User x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(User x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User findById(String x) {
        User u = null;
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("SELECT * FROM user WHERE ID=?");
            stm.setString(1, x);
            ResultSet res = stm.executeQuery();
            if (res.next()){
                u = new User();
                u.setId(x);
                u.setNom(res.getString("NOM"));
                u.setCourriel(res.getString("COURRIEL"));
            }
            else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }

    public User findByEmail(String x) {
        User u = null;
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("SELECT * FROM user WHERE COURRIEL=?");
            stm.setString(1, x);
            ResultSet res = stm.executeQuery();
            if (res.next()){
                u = new User();
                u.setId(res.getString("ID"));
                u.setNom(res.getString("NOM"));
                u.setCourriel(res.getString("COURRIEL"));
            }
            else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }
    @Override
    public User findById(int x) {
       return findById(""+x);
    }

    @Override
    public List<User> findAll() {
        List<User> liste = new LinkedList<>();
        User user;
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("SELECT * FROM user");
            ResultSet res = stm.executeQuery();
            while (res.next()){
                user = new User();
                user.setId(res.getString("ID"));
                user.setNom(res.getString("NOM"));
                user.setCourriel(res.getString("COURRIEL"));
                liste.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }

   
    
}
