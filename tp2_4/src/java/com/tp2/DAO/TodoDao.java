/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.DAO;

import com.tp2.model.Todo;
import com.tp2.model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Piaf
 */
public class TodoDao extends MyDAO<Todo> {

    @Override
    public boolean create(Todo x) {
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("INSERT INTO todo (`ID`,`NOM`,`USER_ID`, `STATUT`) "
                    + "VALUES (?,?, ?,?)");
            stm.setString(1, x.getId());
            stm.setString(2, x.getNom());
            stm.setString(3, x.getUser_id());
            stm.setString(4, x.getStatut().toString());
            return stm.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }

    @Override
    public boolean delete(Todo x) {
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("DELETE FROM todo WHERE ID = '" + x.getId() + "'");
            return stm.executeUpdate() > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(TodoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(Todo x) {
        System.out.println("NOM"+x.getNom());
        System.out.println("ID"+x.getId());
        System.out.println("USER"+x.getUser_id());
        System.out.println("Statut"+x.getStatut());
        System.out.println("======================================================");
        try {
            String req = "UPDATE todo SET STATUT = ? WHERE ID = ?";
            PreparedStatement stm = connexion.getInstance().prepareStatement(req);
//            PreparedStatement stm = connexion.getInstance().prepareStatement("UPDATE todo SET NOM = '" + x.getNom() + "'"
//                    + "USER_ID = '" + x.getUser_id() + "',"
//                    + "STATUT = '" + x.getStatut() + "'"
//                    + " WHERE ID = '" + x.getId() + "'");
            stm.setString(1, x.getStatut().toString());
            stm.setString(2, x.getId());
            return stm.executeUpdate() > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(TodoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Todo findById(String x) {
        Todo t = null;
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("SELECT * FROM todo WHERE ID=?");
            stm.setString(1, x);
            ResultSet res = stm.executeQuery();
            if (res.next()) {
                t = new Todo();
                t.setId(x);
                t.setNom(res.getString("NOM"));
                t.setUser_id(res.getString("USER_ID"));
                t.setStatut(res.getString("STATUT"));
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TodoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;
    }

    @Override
    public Todo findById(int x) {
        return findById("" + x);
    }

    @Override
    public List<Todo> findAll() {
        List<Todo> liste = new LinkedList<>();
        Todo t;
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("SELECT * FROM todo");
            ResultSet res = stm.executeQuery();
            while (res.next()) {
                t = new Todo();
                t.setId(res.getString("ID"));
                t.setNom(res.getString("NOM"));
                t.setStatut(res.getString("USER_ID"));
                t.setStatut(res.getString("STATUT"));
                liste.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    public List<Todo> findAllTachesParUser(String x) {
        List<Todo> liste = new LinkedList<>();
        Todo t;
        try {
            PreparedStatement stm = connexion.getInstance().prepareStatement("SELECT * FROM todo WHERE USER_ID=?");
            
            stm.setString(1, x);
            ResultSet res = stm.executeQuery();
            while (res.next()) {
                t = new Todo();
                t.setId(res.getString("ID"));
                t.setNom(res.getString("NOM"));
                t.setStatut(res.getString("USER_ID"));
                t.setStatut(res.getString("STATUT"));
                liste.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
}
