/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.services;

import com.tp2.DAO.UserDao;
import com.tp2.model.User;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author NelletPiaf
 */
public class UsrService {

    UserDao dao;

    public void setDao(UserDao dao) {
        this.dao = dao;
    }

    public boolean addUser(User user) {
        return dao.create(user);
    }

    public List<String> getUserListe() {
        List<String> liste = new LinkedList<>();
        ListIterator<User> iterateur = dao.findAll().listIterator();

        while (iterateur.hasNext()) {
            liste.add(iterateur.next().getNom());
        }
        return liste;
    }

    public List<User> getAllusers() {
        List<User> listeUser = new LinkedList<User>();
        ListIterator<User> iterator = dao.findAll().listIterator();
        while (iterator.hasNext()) {
            listeUser.add(iterator.next());
        }
        return listeUser;
    }

    public User getUser(String id) {
        User u = new User();
        try {
            u = dao.findById(id);
        } catch (Exception e) {
            System.out.println("Exeception" + e.getMessage());
        }
        return u;
    }

    public User getUserParCourriel(String id) {
        User u = new User();
        try {
            u = dao.findByEmail(id);
        } catch (Exception e) {
            System.out.println("Exeception" + e.getMessage());
        }
        return u;

    }

    public boolean emailValidation(String x) {
        boolean v = false;
        List<String> liste = new LinkedList<>();
        ListIterator<User> iterateur = dao.findAll().listIterator();
        while (iterateur.hasNext()) {
            liste.add(iterateur.next().getCourriel());
        }
        for (int i = 0; i < liste.size(); i++) {
            if (x.equals(liste.get(i))) {
                v = true;
            }
        }
        return v;
    }
}
