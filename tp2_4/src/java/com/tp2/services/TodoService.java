/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tp2.services;

import com.tp2.DAO.TodoDao;
import com.tp2.model.Todo;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Piaf
 */
public class TodoService {

    TodoDao dao;

    public void setDao(TodoDao dao) {
        this.dao = dao;
    }

    public boolean addProjet(Todo projet) {
        return dao.create(projet);
    }

    public List<String> getProjetListe() {
        List<String> list = new LinkedList<>();
        ListIterator<Todo> iterateur = dao.findAll().listIterator();

        while (iterateur.hasNext()) {
            list.add(iterateur.next().getNom());
        }
        return list;
    }
    public Todo getProjet(String id){
                Todo t=new Todo();
        try {
            t=dao.findById(id);
        } catch (Exception e) {
            System.out.println("Exeception"+e.getMessage());
        }
        return t;
    }
    public List<Todo> getTachesParID(String id){
        List<Todo> list=new LinkedList<>();
        ListIterator<Todo> iterateur = dao.findAllTachesParUser(id).listIterator();
         while (iterateur.hasNext()) {
            list.add(iterateur.next());
        }
        return list;       
    }
    
    public boolean setTache(Todo t){
        
        return dao.update(t);  
    }
            
            
}
