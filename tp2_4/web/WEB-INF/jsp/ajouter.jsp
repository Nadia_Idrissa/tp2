<%-- 
    Document   : ajouter
    Created on : 13-Dec-2019, 8:50:32 PM
    Author     : nadym
--%>

<%@page import="com.tp2.model.Etat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajouter Tache</title>
        <title>JSP Page</title>
        <link rel="stylesheet" href="./resources/style.css" type="text/css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/LogStyle.css"/>

    </head>
    <style>
        h1{
            color: antiquewhite;
        }
    </style>
    <body>

        <h1>Ajouter une nouvelle tache</h1>

        <%
            if ((request.getSession().getAttribute("connected") == null) || ((boolean) request.getSession().getAttribute("connected") == false)) {%>

        <jsp:include page="navlogin.jsp" />

        <% } else {%>

        <%}%>

        <br>
        <br>
        <br>
        <h1 style="padding-left: 35%">Nouvelle Tâche!</h1>

        <form:form method="post" name="" modelAttribute="formTache" action="./ajouter">
            <form:input type="text" path="nom"  placeholder="Entrez le nom de la tâche" required="required"/>
            
            <c:set var="enumValues" value="<%=Etat.values()%>"/>
            <form:select path="etat">
                <form:options items="${enumValues}" />               
            </form:select>
            <input type="submit" value="Ajouter">
        </form:form>
             <h3 style="padding-left: 2%"><a href="add"/>Retour</a></h3>
    </body>
</html>
