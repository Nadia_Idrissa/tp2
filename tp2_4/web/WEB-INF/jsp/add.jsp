<%-- 
    Document   : home
    Created on : 27-Nov-2017, 9:48:48 AM
    Author     : Moumene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="./resources/style.css" type="text/css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./resources/LogStyle.css"/>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    </head>
    <style>
        .tittre{
            color: wheat;

            padding-left: 1%;
            .modll{
                padding-bottom: 50% !important;
            }
        }
    </style>
    <body>
        <jsp:include page="navlogin.jsp" />
        <%
            if ((request.getSession().getAttribute("connected") == null) || ((boolean) request.getSession().getAttribute("connected") == false)) {%>

        <jsp:include page="navlogin.jsp" />

        <% } else {%>

        <%}%>
        <br>
        <br>
        <br>

        <div class="wrapper fadeInDown modll">
            <div class="tittre">
                <h1>Bienvenue ${mycourriel}</h1>        
            </div>
            <div id="formContent">               
                <div class="fadeIn first">
                     
                    <table>
                        <h3>Mes taches</h3> 
                       
                        <ol>
                            <c:forEach items="${tachesParUser}" var="unTodo">
                                <li><a href="modif?id=<c:out value="${unTodo.id}" />"><c:out value="${unTodo.nom}" /></a>  <c:out value="${unTodo.statut}" /></li>
                            </c:forEach>
                        </ol>
                        <a href="./ajouter" type="button"class="btn btn-success">Nouvelle tâche</a>
                    </table>
                </div>
            </div>
        </div> 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </body>
</html>
