<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="?action=index"> Gestion de t�ches <span class="sr-only">(current)</span></a>                          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav nav-tabs">        
                <c:if test="${connecte eq null}">
                    <li class="nav-item">
                        <a class="nav-link " href="${pageContext.request.servletContext.contextPath}"> Login</a>
                    </li> 
                </c:if> 
                <c:if test="${connecte ne null}">
                    <li class="nav-item">
                        <a class="nav-link " href="./deconnecter"> Log Out</a>
                    </li> 
                </c:if> 


            </ul>
        </div>
    </div>
</nav>